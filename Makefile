CC=		tcc
CFLAGS=	-O3 -lm -lraylib -lX11 -lGL

all: link clean strip

pre:
	rm -rf build
	mkdir -p build

main: pre
	${CC} -c src/main.c -o build/main.o

gruj: pre
	${CC} -c src/gruj.c -o build/gruj.o 

util: pre
	${CC} -c src/util.c -o build/util.o

link: main gruj util
	${CC} -o xkeymapper build/*.o ${CFLAGS}

strip: link
	strip xkeymapper

clean:
	rm -rf build
