# xkeymapper

xkeymapper is a simple tool to manage X11 keyboard layouts in a handy GUI.

It is written in C using my own toolkit that uses [raylib](https://raylib.com) for drawing, because GTK is confusing.

### How does this work?

It simply calls "setxkbmap" with the appropriate keymap.

### Things to add

- saving to a file in the home directory instead of current directory
- scrolling through the keymap list. I have no idea who has so much layouts but anyway
- release GRUJ as a separate library
- fix removing keymaps

### State

Yet there are only few keymaps, no scrolling etc.

There is still much to do.

### Building

Make sure you have 'setxkbmap', 'tcc' and 'raylib' installed.

```bash
make
```
