#include <raylib.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "include/gruj.h"
#include "include/util.h"

void gruj_instance_init(gruj_instance* g)
{
	g->exit_key = 0;
	g->page = 0;
	g->backgroundColour = RAYWHITE;
}
void gruj_instance_update(gruj_instance* g)
{
	for (int i = 0; i < g->widgetsCount; i++)
	{
		gruj_generic_component* ggc = (gruj_generic_component*)g->_widgets[i];
		
		if (ggc->page == g->page)
			ggc->update(ggc);
	}
}
void gruj_instance_paint(gruj_instance* g)
{
	ClearBackground(g->backgroundColour);
	
	for (int i = 0; i < g->widgetsCount; i++)
	{
		gruj_generic_component* ggc = (gruj_generic_component*)g->_widgets[i];
		
		if (ggc->page == g->page)
			ggc->paint(ggc);
	}
}
void gruj_instance_start(gruj_instance* g)
{
	SetTraceLogLevel(LOG_ERROR);
	InitWindow(g->width, g->height, g->caption);
	SetTargetFPS(g->fps);
	SetExitKey(g->exit_key);
	
	while (!WindowShouldClose())
	{
		gruj_instance_update(g);
		
		BeginDrawing();
		{
			gruj_instance_paint(g);
		}
		EndDrawing();
	}
	
	if (g->on_quit != NULL)
		g->on_quit();
	for (int i = 0; i < g->widgetsCount; i++)
	{
		if (g->_widgets[i] != NULL)
		{
			gruj_generic_component* ggc = (gruj_generic_component*)g->_widgets[i];
			
			ggc->destroy(ggc);
		}
	}
	CloseWindow();
}
bool gruj_add_widget(gruj_instance* g, void* widget)
{
	if (g->widgetsCount >= GRUJ_MAX_WIDGETS)
		return false;
	
	g->_widgets[g->widgetsCount] = widget;
	g->widgetsCount++;
	
	return true;
}
void gruj_remove_widget(gruj_instance* g, void* widget)
{
	// TODO: fix this function
	
	if (g->widgetsCount <= 0)
		return;
	
	for (int i = 0; i < g->widgetsCount; i++)
		if (g->_widgets[i] == widget)
		{
			g->_widgets[i] = NULL;
			g->widgetsCount--;
		}
}
void gruj_remove_all(gruj_instance* g)
{
	for (int i = 0; i < g->widgetsCount; i++)
		g->_widgets[i] = NULL;
	
	g->widgetsCount = 0;
}

// gruj_button

void gruj_button_init(gruj_button* b)
{
	b->type = GRUJ_TYPE_BUTTON;
	b->on_hover = false;
	b->clicked = false;
	b->page = 0;
	
	b->_active_bg = LIGHTGRAY;
	b->_inactive_bg = GRAY;
	b->text_colour = BLACK;
	
	// initialize the functions
	b->update = &gruj_button_update;
	b->paint = &gruj_button_paint;
	b->destroy = &gruj_button_free;
}
void gruj_button_update(void* _b)
{
	gruj_button* b = (gruj_button*)_b;
	
	int mx = GetMousePosition().x;
	int my = GetMousePosition().y;
	
	if (mx >= b->x &&
		my >= b->y &&
		mx <= b->x + b->width &&
		my <= b->y + b->height)
	{
		b->on_hover = true;
		
		if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT))
		{
			b->clicked = true;
			b->clickedWidth = MOUSE_BUTTON_LEFT;
		}
		if (IsMouseButtonPressed(MOUSE_BUTTON_RIGHT))
		{
			b->clicked = true;
			b->clickedWidth = MOUSE_BUTTON_RIGHT;
		}
		if (IsMouseButtonPressed(MOUSE_BUTTON_MIDDLE))
		{
			b->clicked = true;
			b->clickedWidth = MOUSE_BUTTON_MIDDLE;
		}
	} else
	{
		b->on_hover = false;
		b->clicked = false;
	}
	
	if (b->clicked)
	{
		if (b->callback != NULL && b->clickedWidth == MOUSE_BUTTON_LEFT)
			b->callback(b->caption, b->flag);
		if (b->callback_right != NULL && b->clickedWidth == MOUSE_BUTTON_RIGHT)
			b->callback_right(b->caption, b->flag);
		if (b->callback_scroll != NULL && b->clickedWidth == MOUSE_BUTTON_MIDDLE)
			b->callback_scroll(b->caption, b->flag);
		b->clicked = false;
		b->on_hover = false;
	}
}
void gruj_button_paint(void* _b)
{
	gruj_button* b = (gruj_button*)_b;
	
	DrawRectangle(
		b->x,
		b->y,
		b->width,
		b->height,
		b->on_hover ? b->_active_bg : b->_inactive_bg);
	
	DrawText(
		b->caption,
		b->x + (b->width / 2 - (MeasureText(b->caption, b->font_size) / 2)),
		b->y + (b->height / 2 - b->font_size / 2),
		b->font_size,
		b->text_colour);
}
void gruj_button_free(void* _b)
{
	gruj_button* b = (gruj_button*)_b;
	
	if (b->caption != NULL)
		free(b->caption);
}

// gruj_textbox

void gruj_textbox_init(gruj_textbox* t, gruj_instance* g, int max_length)
{
	t->type = GRUJ_TYPE_TEXTBOX;
	t->max_length = max_length;
	t->on_hover = false;
	t->clicked = false;
	t->page = 0;
	t->g = g;
	
	t->_active_bg = LIGHTGRAY;
	t->_inactive_bg = GRAY;
	t->_border_colour = BLACK;
	t->text_colour = BLACK;
	t->border_px = 2;
	t->active = false;
	
	t->text = (char*)malloc((max_length + 1) * sizeof(char));
	for (int i = 0; i < max_length; i++)
		t->text[i] = ' ';
	t->text[max_length] = '\0';
	t->text_length = 0;
	
	// initialize the functions
	t->update = &gruj_textbox_update;
	t->paint = &gruj_textbox_paint;
	t->destroy = &gruj_textbox_free;
}
void gruj_textbox_update(void* _t)
{
	gruj_textbox* t = (gruj_textbox*)_t;
	gruj_instance* g = t->g;
	
	int mx = GetMousePosition().x;
	int my = GetMousePosition().y;
	
	if (mx >= t->x &&
		my >= t->y &&
		mx <= t->x + t->width &&
		my <= t->y + t->height)
	{
		t->on_hover = true;
		
		if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT))
			t->clicked = true;
	} else
	{
		t->on_hover = false;
		t->clicked = false;
	}
	
	if (t->clicked)
	{
		t->active = !t->active;
		t->clicked = false;
		t->on_hover = false;
	}
	
	if (t->active)
	{
		// disable any other active textboxes
		for (int i = 0; i < g->widgetsCount; i++)
		{
			gruj_generic_component* dontBreak = g->_widgets[i];
			
			if (dontBreak->type == GRUJ_TYPE_TEXTBOX)
			{
				gruj_textbox* nemesis = g->_widgets[i];
			
				if (t != nemesis && nemesis->active)
					nemesis->active = false;
			}
		}
		
		int ch = GetCharPressed();
		
		if (ch >= 32 && ch <= 125 && t->text_length < t->max_length)
		{
			t->text[t->text_length] = ch;
			t->text_length++;
		}
		
		if (IsKeyPressed(KEY_BACKSPACE) && t->text_length > 0)
		{
			t->text_length--;
			t->text[t->text_length] = ' ';
		}
	}
}
void gruj_textbox_paint(void* _t)
{
	gruj_textbox* t = (gruj_textbox*)_t;
	
	DrawRectangle(
		t->x,
		t->y,
		t->width,
		t->height,
		t->_border_colour);
	DrawRectangle(
		t->x + t->border_px,
		t->y + t->border_px,
		t->width - t->border_px * 2,
		t->height - t->border_px * 2,
		t->active ? t->_active_bg : t->_inactive_bg);
	char* trimmedText = strdup(t->text);
	trim_string(trimmedText, t->text_length);
	DrawText(
		t->text,
		t->x + (t->width / 2 - (MeasureText(trimmedText, t->font_size) / 2)),
		t->y + (t->height / 2 - t->font_size / 2),
		t->font_size,
		t->text_colour);
	free(trimmedText);
}
void gruj_textbox_free(void* _t)
{
	gruj_textbox* t = (gruj_textbox*)_t;
	
	if (t->text != NULL)
		free(t->text);
}

// gruj_label

void gruj_label_init(gruj_label* l)
{
	l->type = GRUJ_TYPE_LABEL;
	l->font_colour = BLACK;
	l->font_size = 12;
	
	// initialize the functions
	l->update = &gruj_label_update;
	l->paint = &gruj_label_paint;
	l->destroy = &gruj_label_free;
}
void gruj_label_update(void* _l)
{
	gruj_label* l = (gruj_label*)_l;
}
void gruj_label_paint(void* _l)
{
	gruj_label* l = (gruj_label*)_l;
	
	DrawText(
		l->caption,
		l->x,
		l->y,
		l->font_size,
		l->font_colour);
}
void gruj_label_free(void* _l)
{
	gruj_label* l = (gruj_label*)_l;
	
	if (l->caption != NULL)
		free(l->caption);
}
