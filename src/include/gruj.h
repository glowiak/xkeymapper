#pragma once

#include <raylib.h>
#include <stdbool.h>

#define GRUJ_MAX_WIDGETS 256

typedef struct
{
	unsigned int width;
	unsigned int height;
	unsigned int fps;
	char* caption;
	Color backgroundColour;
	int exit_key;
	int page;
	
	void* _widgets[GRUJ_MAX_WIDGETS];
	unsigned int widgetsCount;
	void (*on_quit)(void);
} gruj_instance;

void gruj_instance_init(gruj_instance* g);
void gruj_instance_update(gruj_instance* g);
void gruj_instance_paint(gruj_instance* g);
void gruj_instance_start(gruj_instance* g);

bool gruj_add_widget(gruj_instance* g, void* widget);
void gruj_remove_widget(gruj_instance* g, void* widget);
void gruj_remove_all(gruj_instance* g);

#define GRUJ_TYPE_BUTTON 0
#define GRUJ_TYPE_TEXTBOX 1
#define GRUJ_TYPE_LABEL 2

typedef struct
{
	unsigned int type;
	int x;
	int y;
	int page;
	
	void (*update)(void*);
	void (*paint)(void*);
	void (*destroy)(void*);
} gruj_generic_component;

#define GRUJ_BUTTON_TEXT 0
#define GRUJ_BUTTON_IMAGE 1

typedef struct // extends gruj_generic_component
{
	// inherited attributes
	unsigned int type;
	int x;
	int y;
	int page;
	
	void (*update)(void*);
	void (*paint)(void*);
	void (*destroy)(void*);
	
	// own attributes
	unsigned int buttonType;
	unsigned int width;
	unsigned int height;
	unsigned int font_size;
	char* caption;
	bool on_hover;
	bool clicked;
	int flag;
	int clickedWidth;
	
	Color _active_bg;
	Color _inactive_bg;
	Color text_colour;
	
	void (*callback)(const char*, const int);
	void (*callback_right)(const char*, const int);
	void (*callback_scroll)(const char*, const int);
} gruj_button;

void gruj_button_init(gruj_button* b);
void gruj_button_update(void* _b);
void gruj_button_paint(void* _b);
void gruj_button_free(void* _b);

typedef struct // extends gruj_generic_component
{
	// inherited attributes
	unsigned int type;
	int x;
	int y;
	int page;
	
	void (*update)(void*);
	void (*paint)(void*);
	void (*destroy)(void*);
	
	// own attributes
	unsigned int width;
	unsigned int height;
	bool on_hover;
	bool clicked;
	int max_length;
	char* text;
	unsigned int text_length;
	int border_px;
	bool active;
	int font_size;
	gruj_instance* g;
	
	Color _active_bg;
	Color _inactive_bg;
	Color _border_colour;
	Color text_colour;
} gruj_textbox;

void gruj_textbox_init(gruj_textbox* t, gruj_instance* g, int max_length);
void gruj_textbox_update(void* _t);
void gruj_textbox_paint(void* _t);
void gruj_textbox_free(void* _t);

typedef struct // extends gruj_generic_component
{
	// inherited attributes
	unsigned int type;
	int x;
	int y;
	int page;
	
	void (*update)(void*);
	void (*paint)(void*);
	void (*destroy)(void*);
	
	// own attributes
	char* caption;
	int font_size;
	Color font_colour;
} gruj_label;

void gruj_label_init(gruj_label* l);
void gruj_label_update(void* _l);
void gruj_label_paint(void* _l);
void gruj_label_free(void* _l);
