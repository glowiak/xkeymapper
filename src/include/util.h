#pragma once

#include <stdbool.h>

// remove spaces from the end of a string
void trim_string(char* s, unsigned int target_length);

bool file_exists(const char* path);
void str_remchar(char* str, char to_remove, char to_replace);
