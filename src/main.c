#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "include/gruj.h"
#include "include/util.h"

#define WIDTH 400
#define HEIGHT 500
#define CAPTION "xkeymapper"
#define SAVEFILE "save.dat"

static gruj_instance gruj;

static gruj_button b_add;

#define MAX_KEYMAPS 25
#define KEYMAP_SEARCH_PATHS (char*[]) \
	{ \
		"/usr/share/X11/xkb/symbols", \
		"/usr/local/share/X11/xkb/symbols", \
	}

static char* keymaps[MAX_KEYMAPS];
static char* keymapDescriptions[MAX_KEYMAPS];
static gruj_button keymapButtons[MAX_KEYMAPS];
static int keymapCount;

static gruj_textbox tb_path;
static gruj_textbox tb_description;
static gruj_button b_p1_back;
static gruj_label l_p1_t0;
static gruj_label l_p1_t1;

int main(int argc, char** argv);
void callback(const char* caption, const int flag);
void callback_right(const char* caption, const int flag);
void refresh_ui();
bool add_keymap(const char* name, const char* description);
bool remove_keymap(int id);
void rebuild_array_tree();
bool load(const char* path);
bool save(const char* path);
void on_quit();
bool keymap_exists(const char* name);

int main(int argc, char** argv)
{
	gruj_instance_init(&gruj);
	
	gruj.width = WIDTH;
	gruj.height = HEIGHT;
	gruj.fps = 30;
	gruj.caption = strdup(CAPTION);
	gruj.exit_key = 0;
	gruj.on_quit = &on_quit;
	
	if (file_exists(SAVEFILE))
	{
		// TODO: load the file
		if (!load(SAVEFILE))
		{
			// TODO: print the error
		}
	}
	
	refresh_ui();
	
	gruj_instance_start(&gruj);
	
	return 0;
}
void callback(const char* caption, const int flag)
{
	if (flag == -1) // add
	{	
		gruj.page = 1;
	}
	if (flag >= 0)
	{
		printf("Set keymap to %s\n", keymaps[flag]);
		
		char* toPrint = (char*)malloc(sizeof(char*) *
			(
				strlen("setxkbmap ") +
				strlen(keymaps[flag]) +
				1
			)
		);
		sprintf(toPrint, "setxkbmap %s", keymaps[flag]);
		
		int result = system(toPrint);
		
		if (result != 0)
		{
			// handle the error
			printf("error code: %d\n", result);
		}
		
		free(toPrint);
	}
	
	// page 1
	if (flag == -100)
	{
		if (tb_path.text_length > 0 && tb_description.text_length > 0)
		{
			char* newKeymap = strdup(tb_path.text);
			trim_string(newKeymap, tb_path.text_length);
			
			char* newDescription = strdup(tb_description.text);
			trim_string(newDescription, tb_description.text_length);
			
			if (keymap_exists(newKeymap))
				add_keymap(newKeymap, newDescription);
			
			free(newKeymap);
			free(newDescription);
		} else
		{
			// TODO: handle the error
		}
		
		gruj.page = 0;
		
		if (!save(SAVEFILE))
		{
			// TODO: bail error
		}
	}
	
	refresh_ui();
}
void callback_right(const char* caption, const int flag)
{
	if (flag >= 0)
	{
		remove_keymap(flag);
		rebuild_array_tree();
		refresh_ui();
		
		if (!save(SAVEFILE))
		{
			// TODO: bail error
		}
	}
}
void refresh_ui()
{
	gruj_remove_all(&gruj);
	
	gruj_button_init(&b_add);
	
	b_add.caption = strdup("Add");
	b_add.font_size = 20;
	b_add.x = 5;
	b_add.y = 5;
	b_add.width = 50;
	b_add.height = 50;
	b_add.callback = &callback;
	b_add.flag = -1;
	
	// add the keymap buttons
	{
		int bx = 5;
		int by = 100;
		
		int tw = WIDTH - 10;
		int th = 25;
		
		int tfs = 15;
		
		for (int i = 0; i < keymapCount; i++)
		{
			gruj_button_init(&keymapButtons[i]);
			
			// prepare the caption
			char* toPrint = (char*)malloc(sizeof(char) * (
				strlen(keymaps[i]) +
				strlen(keymapDescriptions[i]) +
				strlen(" ()") +
				1
			));
			sprintf(toPrint, "%s (%s)", keymapDescriptions[i], keymaps[i]);
			
			keymapButtons[i].x = bx;
			keymapButtons[i].y = by;
			keymapButtons[i].caption = strdup(toPrint);
			keymapButtons[i].width = tw;
			keymapButtons[i].height = th;
			keymapButtons[i].font_size = tfs;
			keymapButtons[i].callback = &callback;
			keymapButtons[i].callback_right = &callback_right;
			keymapButtons[i].flag = i;
			
			free(toPrint);
			
			gruj_add_widget(&gruj, &keymapButtons[i]);
			
			by += th;
			by += 5;
		}
	}
	
	gruj_add_widget(&gruj, &b_add);
	
	// page 1 - add keymap
	
	gruj_textbox_init(&tb_path, &gruj, 35);
	
	tb_path.x = 5;
	tb_path.y = 40;
	tb_path.width = WIDTH - 10;
	tb_path.height = 25;
	tb_path.font_size = 15;
	tb_path.page = 1;
	
	gruj_button_init(&b_p1_back);
	
	b_p1_back.page = 1;
	b_p1_back.x = WIDTH / 2 - 50;
	b_p1_back.y = HEIGHT - 30;
	b_p1_back.width = 100;
	b_p1_back.height = 25;
	b_p1_back.caption = strdup("Add");
	b_p1_back.flag = -100;
	b_p1_back.callback = &callback;
	b_p1_back.font_size = 15;
	
	gruj_label_init(&l_p1_t0);
	l_p1_t0.page = 1;
	l_p1_t0.font_size = 18;
	l_p1_t0.caption = strdup("Name of the layout:");
	l_p1_t0.x = WIDTH / 2 - (MeasureText("Name of the layout:", 18) / 2);;
	l_p1_t0.y = 5;
	
	gruj_label_init(&l_p1_t1);
	l_p1_t1.page = 1;
	l_p1_t1.font_size = 18;
	l_p1_t1.caption = strdup("Layout description:");
	l_p1_t1.x = WIDTH / 2 - (MeasureText("Layout description", 18) / 2);
	l_p1_t1.y = 70;
	
	gruj_textbox_init(&tb_description, &gruj, 35);
	tb_description.x = 5;
	tb_description.y = 90;
	tb_description.width = WIDTH - 10;
	tb_description.height = 25;
	tb_description.font_size = 15;
	tb_description.page = 1;
	
	gruj_add_widget(&gruj, &tb_path);
	gruj_add_widget(&gruj, &b_p1_back);
	gruj_add_widget(&gruj, &l_p1_t0);
	gruj_add_widget(&gruj, &l_p1_t1);
	gruj_add_widget(&gruj, &tb_description);
}
bool add_keymap(const char* name, const char* description)
{
	if (keymapCount + 1 >= MAX_KEYMAPS)
		return false;
	
	keymaps[keymapCount] = strdup(name);
	keymapDescriptions[keymapCount] = strdup(description);
	keymapCount++;
			
	return true;
}
bool remove_keymap(int id)
{
	if (id < 0 || id >= MAX_KEYMAPS || keymaps[id] == NULL)
		return false;
	
	free(keymaps[id]);
	free(keymapDescriptions[id]);
	keymapButtons[id].caption = strdup("removed");
	
	printf("remove %d\n", id);
	
	return true;
}
void rebuild_array_tree()
{
	// remove all NULLs from the button array, and shift them
	
	int decby = 0;
	
	for (int i = 0; i < keymapCount; i++)
	{
		if (
			strcmp(keymapButtons[i].caption, "removed") == 0 &&
			(i + 1) < MAX_KEYMAPS &&
			keymapDescriptions[i + 1] != NULL)
		{
			keymaps[i] = strdup(keymaps[i + 1]);
			keymapDescriptions[i] = strdup(keymapDescriptions[i + 1]);
			
			char* pr = strdup(keymapButtons[i + 1].caption);
			
			keymapButtons[i] = keymapButtons[i + 1];
			keymapButtons[i].caption = strdup(pr);
			
			free(pr);
			
			free(keymaps[i + 1]);
			free(keymapDescriptions[i + 1]);
			keymapButtons[i + 1].caption = strdup("removed");
			
			decby++;
		}
		
		keymapCount -= decby;
	}
}
bool load(const char* path)
{
	FILE* fp = fopen(path, "r");
	
	if (fp == NULL)
		return false;
	
	// load the file
	char* line = NULL;
	size_t len = 0;
	int llen = getline(&line, &len, fp);
	
	int keymaps_ = (int)strtol(line, NULL, 10);
	
	free(line);
	
	for (int i = 0; i < keymaps_; i++)
	{
		line = NULL;
		len = 0;
		
		llen = getline(&line, &len, fp);
		str_remchar(line, '\n', '\0');
		keymaps[i] = strdup(line);
		
		llen = getline(&line, &len, fp);
		str_remchar(line, '\n', '\0');
		keymapDescriptions[i] = strdup(line);
		
		free(line);
	}
	keymapCount = keymaps_;
	
	fclose(fp);
	
	return true;
}
bool save(const char* path)
{
	FILE* fp = fopen(path, "w");
	
	if (fp == NULL)
		return false;
	
	fprintf(fp, "%d\n", keymapCount);
	for (int i = 0; i < keymapCount; i++)
	{
		fprintf(fp, "%s\n", keymaps[i]);
		fprintf(fp, "%s\n", keymapDescriptions[i]);
	}
	
	fclose(fp);
	
	return false;
}
void on_quit()
{
	if (!save(SAVEFILE))
		printf("Could not save.\n");
	
	for (int i = 0; i < MAX_KEYMAPS; i++)
	{
		if (keymaps[i] != NULL)
			free(keymaps[i]);
		if (keymapDescriptions[i] != NULL)
			free(keymapDescriptions[i]);
	}
}
bool keymap_exists(const char* name)
{
	bool w = false;
	
	for (int i = 0; i < 2; i++)
	{
		char* tp = (char*)malloc(sizeof(char) * (strlen(name) + strlen(KEYMAP_SEARCH_PATHS[i]) + 1));
		
		sprintf(tp, "%s/%s", KEYMAP_SEARCH_PATHS[i], name);
		
		if (file_exists(tp))
		{
			w = true;
			printf("%s exists.\n", tp);
		}
		
		free(tp);
	}
	
	return w;
}
