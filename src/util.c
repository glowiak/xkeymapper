#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

#include "include/util.h"

void trim_string(char* s, unsigned int target_length)
{
	s = (char*)realloc(s, sizeof(char) * (target_length + 1));
	s[target_length] = '\0';
}
bool file_exists(const char* path)
{
	FILE* fp = fopen(path, "r");
	
	if (fp == NULL)
		return false;
	
	fclose(fp);
	
	return true;
}
void str_remchar(char* str, char to_remove, char to_replace)
{
	for (int i = 0; i < strlen(str); i++)
	{
		if (str[i] == to_remove)
			str[i] = to_replace;
	}
}
